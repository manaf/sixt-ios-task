//
//  MapSceneUITests.swift
//  SixtUITests
//
//  Created by Manaf Abd Alrahim on 1/8/21.
//

import XCTest

class MapSceneUITests: XCTestCase {

	override func setUp() {
		self.continueAfterFailure = false
		XCUIApplication().launch()
	}
	
    func testMapSceneNavigations() throws {
		let app = XCUIApplication()
		
		let map = app.otherElements[AccessibilityIdentifier.mapViewController]
		
		let mapExists = map.waitForExistence(timeout: 2)
		XCTAssertTrue(mapExists)
		
		let carList = app.otherElements[AccessibilityIdentifier.carListViewController]
		let carListExists = carList.waitForExistence(timeout: 5)
		XCTAssertTrue(carListExists)
		
		carList.tables.cells.firstMatch.tap(timeout: 2)
		
		sleep(2)
		
		map.collectionViews.firstMatch.swipeLeft()
		sleep(1)
		map.collectionViews.firstMatch.swipeRight()
		
		let firstAnnotation = app.descendants(matching: .other).matching(identifier: AccessibilityIdentifier.mapAnnotationView).element(boundBy: 0)
		
		firstAnnotation.tap(timeout: 2)
		sleep(2)
		
		let secondAnnotation = app.descendants(matching: .other).matching(identifier: AccessibilityIdentifier.mapAnnotationView).element(boundBy: 1)
		
		secondAnnotation.tap(timeout: 2)
		sleep(2)
		
		
    }

}
