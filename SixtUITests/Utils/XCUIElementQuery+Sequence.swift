//
//  XCUIElementQuery+Sequence.swift
//  SixtUITests
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import XCTest

extension XCUIElementQuery: Sequence {
	
	public typealias Iterator = AnyIterator<XCUIElement>
	
	public func makeIterator() -> Iterator {
		var index = UInt(0)
		return AnyIterator {
			guard index < self.count else { return nil }

			let element = self.element(boundBy: Int(index))
			index = index + 1
			return element
		}
	}
}
