//
//  XCUIElement+Tap.swift
//  SixtUITests
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import XCTest

extension XCUIElement {
	
	@discardableResult
	func tap(timeout: TimeInterval, assertNotFound: Bool = false) -> Bool {
		if self.waitForExistence(timeout: timeout) {
			if assertNotFound {
				XCTAssertTrue(self.exists, self.identifier + " doesn't exist")
			}
				
			if self.exists {
				self.tap()
				return true
			}
		}
		return false
	}
}
