# Sixt iOS Coding Task

A sample native iOS application that fetches a list of cars from an API and displays it in an MKMapView, a UITableView, and a UICollectionView

## Structure and Architecture

- Clean Architecture
- MVVM architectural pattern
- Dependency Injection
- Data Transfer Objects (DTO)
 
## Implemented Concepts
- Dark Mode
- Unit Test (via XCTest)
- UI Test (via XCUITests)

## Dependencies

- [SENetworking](https://github.com/kudoleh/SENetworking): A light-weight NSURLSession wrapper. Used to handle API connection.
- [Kingfisher](https://github.com/onevcat/Kingfisher): An image fetching and caching library. Used to streamline the handling of remote images.
- [FloatingPanel](https://github.com/SCENEE/FloatingPanel): A UI component that displays content in parallel to the background, similar to the components used in Apple's Maps and Stocks apps. Used to display the list of cars in parallel with the Map.
- [Fastlane](https://github.com/kudoleh/SENetworking): Used for automating the test process.

## Tools
- Xcode 12.3

## Running Tests (via Fastlane)

```bash
fastlane test
```