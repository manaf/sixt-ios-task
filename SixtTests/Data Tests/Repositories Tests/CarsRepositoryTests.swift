//
//  CarsRepositoryTests.swift
//  SixtTests
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//
import XCTest
import SENetworking
@testable import Sixt

class CarsRepositoryTests: XCTestCase {

	let mockCarsDTO: [CarDTO] = [
		CarDTO(id: "id1",
			   modelIdentifier: "modelIdentifier1",
			   modelName: "modelName1",
			   name: "name1",
			   make: "make1",
			   group: "group1",
			   color: "color1",
			   series: "series1",
			   fuelType: .petrol,
			   fuelLevel: 0.5,
			   transmission: .manual,
			   licensePlate: "licensePlate1",
			   latitude: 20,
			   longitude: 20,
			   innerCleanliness: .clean,
			   carImageUrl: nil),
		CarDTO(id: "id2",
			   modelIdentifier: "modelIdentifier2",
			   modelName: "modelName2",
			   name: "name2",
			   make: "make2",
			   group: "group2",
			   color: "color2",
			   series: "series2",
			   fuelType: .diesel,
			   fuelLevel: 0.5,
			   transmission: .automatic,
			   licensePlate: "licensePlate2",
			   latitude: 25,
			   longitude: 25,
			   innerCleanliness: .regular,
			   carImageUrl: nil)
	]
	
	class DataTransferServiceMock<T: Decodable>: DataTransferService {
		
		enum Mode<T> {
			case success(T)
			case fail
		}
		
		private var mode: Any
		
		init<T>(mode: Mode<T>) {
			self.mode = mode
		}
		
		public func request<T: Decodable, E: ResponseRequestable>(with endpoint: E,
																  completion: @escaping CompletionHandler<T>) -> NetworkCancellable? where E.Response == T {
			
			guard let mode = mode as? Mode<T> else { return nil}
				   
			switch mode {
			case let .success(value):
				completion(.success(value))
			case .fail:
				completion(.failure(.noResponse))
			}
			
			return nil
		}
		
		func request<E>(with endpoint: E, completion: @escaping CompletionHandler<Void>) -> NetworkCancellable? where E : ResponseRequestable, E.Response == Void {
			guard let mode = mode as? Mode<T> else { return nil}
				   
			switch mode {
			case .success(_):
				completion(.success(()))
			case .fail:
				completion(.failure(DataTransferError.noResponse))
			}
			return nil
		}
		
		
		
	}
	
	func testCarsRepositorySuccess() throws {
		let repository = CarsRepositoryImpl(dataTransferService: DataTransferServiceMock<CarDTO>(mode: DataTransferServiceMock.Mode.success(self.mockCarsDTO)))
		
		repository.fetchCarsList { result in
			switch result {
			case .success(let cars):
				XCTAssertTrue(cars.count == self.mockCarsDTO.count)
			case .failure(_):
				XCTAssertTrue(true)
			}
		}
	}
	
	func testFetchCarsUseCaseFailure() throws {
		let repository = CarsRepositoryImpl(dataTransferService: DataTransferServiceMock<CarDTO>(mode: DataTransferServiceMock.Mode<CarDTO>.fail))
		
		repository.fetchCarsList { result in
			switch result {
			case .success(_):
				XCTAssertTrue(true)
				
			case .failure(_):
				break
			}
		}
	}

}
