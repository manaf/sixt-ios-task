//
//  MapViewModelTest.swift
//  SixtTests
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import XCTest
@testable import Sixt

class MapViewModelTest: XCTestCase {
	
	enum CarsRepositoryErrorMock: Error {
		case fetchingFailed
	}
	
	final class FetchCarsUseCaseSuccessfulMock: FetchCarsUseCase {
		func execute(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable? {
			let mockCars: [Car] = [
				Car(id: "id1",
					modelIdentifier: "modelIdentifier1",
					modelName: "modelName1",
					name: "name1",
					make: "make1",
					group: "group1",
					color: "color1",
					series: "series1",
					fuelType: .petrol,
					fuelLevel: 0.5,
					transmission: .manual,
					licensePlate: "licensePlate1",
					latitude: 20,
					longitude: 20,
					innerCleanliness: .clean,
					carImageUrl: nil),
				Car(id: "id2",
					modelIdentifier: "modelIdentifier2",
					modelName: "modelName2",
					name: "name2",
					make: "make2",
					group: "group2",
					color: "color2",
					series: "series2",
					fuelType: .diesel,
					fuelLevel: 0.5,
					transmission: .automatic,
					licensePlate: "licensePlate2",
					latitude: 25,
					longitude: 25,
					innerCleanliness: .regular,
					carImageUrl: nil)
				]
			
			completion(.success(mockCars))
			
			return nil
		}
	}
	
	final class FetchCarsUseCaseFailureMock: FetchCarsUseCase {
		func execute(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable? {

			completion(.failure(CarsRepositoryErrorMock.fetchingFailed))
			return nil
		}
	}
	
	func testMapViewModelSuccess() throws {
		let viewModel = MapViewModelImp(fetchCarsUseCase: FetchCarsUseCaseSuccessfulMock())
		viewModel.load()
		
		XCTAssertTrue(viewModel.items.value.count == 2)
		XCTAssertTrue(viewModel.selectedItemIndex == nil)
		XCTAssertTrue(viewModel.selectedItem.value == nil)
		XCTAssertTrue(viewModel.isListShown.value == true)
		
		viewModel.didSelect(item: viewModel.items.value[1])
		
		XCTAssertTrue(viewModel.selectedItemIndex == 1)
		XCTAssertTrue(viewModel.selectedItem.value == viewModel.items.value[1])
		XCTAssertTrue(!viewModel.isListShown.value)
		
		viewModel.didDeselect()
		
		XCTAssertTrue(viewModel.selectedItemIndex == nil)
		XCTAssertTrue(viewModel.selectedItem.value == nil)
		XCTAssertTrue(viewModel.isListShown.value == true)
		
	}
	
	func testMapViewModelFailure() throws {
		let viewModel = MapViewModelImp(fetchCarsUseCase: FetchCarsUseCaseFailureMock())
		viewModel.load()
		
		XCTAssertTrue(viewModel.items.value.count == 0)
		XCTAssertTrue(!viewModel.error.value.isEmpty)
	}
}
