//
//  FetchCarsUseCaseTests.swift
//  SixtTests
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import XCTest
@testable import Sixt

class FetchCarsUseCaseTests: XCTestCase {

	enum FetchCaseUseCaseErrorMock: Error {
		case fetchingFailed
	}
	
	class RepositoryTaskMock: Cancellable {
		func cancel() {}
	}
	
	final class CarsRepositorySuccessfulMock: CarsRepository {
		func fetchCarsList(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable? {
			let cars: [Car] = [
				Car(id: "id1",
					modelIdentifier: "modelIdentifier1",
					modelName: "modelName1",
					name: "name1",
					make: "make1",
					group: "group1",
					color: "color1",
					series: "series1",
					fuelType: .petrol,
					fuelLevel: 0.5,
					transmission: .manual,
					licensePlate: "licensePlate1",
					latitude: 20,
					longitude: 20,
					innerCleanliness: .clean,
					carImageUrl: nil),
				Car(id: "id2",
					modelIdentifier: "modelIdentifier2",
					modelName: "modelName2",
					name: "name2",
					make: "make2",
					group: "group2",
					color: "color2",
					series: "series2",
					fuelType: .diesel,
					fuelLevel: 0.5,
					transmission: .automatic,
					licensePlate: "licensePlate2",
					latitude: 25,
					longitude: 25,
					innerCleanliness: .regular,
					carImageUrl: nil)
				]
			completion(.success(cars))
			return nil
		}
	}
	
	final class CarsRepositoryFailedMock: CarsRepository {
		func fetchCarsList(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable? {
			completion(.failure(FetchCaseUseCaseErrorMock.fetchingFailed))
			
			return nil
		}
	}
	
    func testFetchCarsUseCaseSuccess() throws {
        let useCase = FetchCarsUseCaseImpl(carsRepository: CarsRepositorySuccessfulMock())
		
		useCase.execute { result in
			switch result {
			
			case .success(let cars):
				XCTAssertTrue(cars.count == 2)
				
			case .failure(_):
				XCTAssertTrue(true)
			}
		}
	}
	
	func testFetchCarsUseCaseFailure() throws {
		let useCase = FetchCarsUseCaseImpl(carsRepository: CarsRepositoryFailedMock())
		
		useCase.execute { result in
			switch result {
			
			case .success(_):
				XCTAssertTrue(true)
				
			case .failure(_):
				break
			}
		}
	}

}
