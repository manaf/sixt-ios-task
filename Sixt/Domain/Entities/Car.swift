//
//  Car.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/8/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import MapKit

struct Car {
	
	let id: String
	let modelIdentifier: String?
	let modelName: String?
	let name: String?
	let make: String?
	let group: String?
	let color: String?
	let series: String?
	let fuelType: FuelType?
	let fuelLevel: Double?
	let transmission: Transmition?
	let licensePlate: String?
	let latitude: Double
	let longitude: Double
	let innerCleanliness: InnerCleanliness?
	let carImageUrl: URL?
	
	enum FuelType: String {
		case petrol
		case diesel
		case electric
	}
	
	enum Transmition: String {
		case manual
		case automatic
	}
	
	enum InnerCleanliness: String {
		case regular
		case clean
		case veryClean
	}
	
}
