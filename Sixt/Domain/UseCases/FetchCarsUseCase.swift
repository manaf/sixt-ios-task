//
//  FetchCarsUseCase.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation

protocol FetchCarsUseCase {
	@discardableResult
	func execute(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable?
}

final class FetchCarsUseCaseImpl: FetchCarsUseCase {
	
	private let carsRepository: CarsRepository
	
	init(carsRepository: CarsRepository) {
		self.carsRepository = carsRepository
	}
	
	@discardableResult
	func execute(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable? {
		return self.carsRepository.fetchCarsList { (result) in
			completion(result)
		}
	}
}
