//
//  CarsRepository.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import SENetworking

protocol CarsRepository {
	@discardableResult
	func fetchCarsList(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable?
}
