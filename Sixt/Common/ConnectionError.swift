//
//  ConnectionError.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import SENetworking

public protocol ConnectionError: Error {
	var isConnectionError: Bool { get }
}

public extension Error {
	var isConnectionError: Bool {
		guard let error = self as? ConnectionError, error.isConnectionError else {
			return false
		}
		return true
	}
}

extension DataTransferError: ConnectionError {
	public var isConnectionError: Bool {
		guard case let DataTransferError.networkFailure(networkError) = self,
			  case .notConnected = networkError else {
				return false
		}
		return true
	}
}
