//
//  AccessibilityIdentifier.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
public class AccessibilityIdentifier {
	static let mapViewController = "AccessibilityIdentifierMapViewController"
	static let mapAnnotationView = "AccessibilityIdentifierMapAnnotationView"
	static let carListViewController = "AccessibilityIdentifierCarListViewController"
}
