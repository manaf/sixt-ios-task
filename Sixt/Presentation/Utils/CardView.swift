//
//  CardView.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit

class CardView: UIView {
	
	//Container background color
	@IBInspectable var containerBackgroundColor : UIColor = UIColor.backgroundColor ?? .systemBackground
	
	//Corner Radius
	@IBInspectable var cornerRadius : CGFloat = 8
	
	//Shadow
	@IBInspectable var shadowEnabled : Bool = true
	@IBInspectable var shadowColor : UIColor = .black
	@IBInspectable var shadowRadius : CGFloat = 3
	@IBInspectable var shadowOpacity : Float = 0.15
	@IBInspectable var shadowOffset: CGSize = CGSize(width: 0, height: 3)
	
	let containerView = UIView()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.layoutView()
		
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.layoutView()
	}
	
	func layoutView() {
		
		layer.backgroundColor = UIColor.clear.cgColor
		if shadowEnabled {
			
			layer.shadowColor = self.shadowColor.cgColor
			layer.shadowOffset = self.shadowOffset
			layer.shadowOpacity = self.shadowOpacity
			layer.shadowRadius = self.shadowRadius
		}
		
		
		self.containerView.layer.cornerRadius = self.cornerRadius
		self.containerView.layer.masksToBounds = true
		self.containerView.backgroundColor = self.containerBackgroundColor
		addSubview(self.containerView)
		self.sendSubviewToBack(self.containerView)
		
		self.containerView.translatesAutoresizingMaskIntoConstraints = false
		
		self.containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
		self.containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
		self.containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
		self.containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
	}
}
