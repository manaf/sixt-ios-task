//
//  UIView+LoadFromNib.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit

extension UIView {
	
	class func fromNib<View: UIView>(bundle: Bundle? = Bundle.main) -> View {
		
		return bundle?.loadNibNamed(String(describing: View.self), owner: nil, options: nil)![0] as! View
	}
}
