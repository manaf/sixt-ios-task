//
//  UIColor+AppColors.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit

extension UIColor {
	
	@nonobjc static let accentColor: UIColor? = {
		return UIColor(named: "AccentColor")
	}()
	
	@nonobjc static let contentColor: UIColor? = {
		return UIColor(named: "ContentColor")
	}()
	
	@nonobjc static let backgroundColor: UIColor? = {
		return UIColor(named: "BackgroundColor") 
	}()
}
