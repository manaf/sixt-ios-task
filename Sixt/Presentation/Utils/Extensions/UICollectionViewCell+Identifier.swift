//
//  UICollectionViewCell+Identifier.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
	
	class var identifier: String {
		return String(describing: self)
	}
}
