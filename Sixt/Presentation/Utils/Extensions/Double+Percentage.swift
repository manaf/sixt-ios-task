//
//  Double.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation

extension Double {
	
	var percentage: Double {
		return self * 100
	}
	
	var percentageString: String {
		return String(format: "%.0f%%", self.percentage)
	}
}
