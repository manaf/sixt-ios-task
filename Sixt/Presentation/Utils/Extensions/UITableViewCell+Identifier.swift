//
//  UITableViewCell+Identifier.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/8/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit

extension UITableViewCell {
	
	class var identifier: String {
		return String(describing: self)
	}
}
