//
//  UIViewController+LoadFromNib.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit

extension UIViewController {
	
	static func instantiateFromNib<T: UIViewController>(_ viewType: T.Type) -> T {
		return T.init(nibName: String(describing: T.self), bundle: nil)
	}

	static func loadFromNib() -> Self {
		return instantiateFromNib(self)
	}
}
