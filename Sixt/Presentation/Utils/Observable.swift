//
//  Observable.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation

public final class Observable<Value> {
	
	struct Observer<Value> {
		
		weak var observer: AnyObject?
		let bind: (Value) -> Void
	}
	
	private var observers = [Observer<Value>]()
	
	public var value: Value {
		didSet {
			notifyObservers()
		}
	}
	
	public init(_ value: Value) {
		self.value = value
	}
	
	public func observe(on observer: AnyObject,
						observerBindBlock: @escaping (Value) -> Void) {
		observers.append(Observer(observer: observer, bind: observerBindBlock))
		observerBindBlock(self.value)
	}
	
	public func remove(observer: AnyObject) {
		observers = observers.filter { $0.observer !== observer }
	}
	
	private func notifyObservers() {
		for observer in observers {
			DispatchQueue.main.async {
				observer.bind(self.value)
			}
		}
	}
}
