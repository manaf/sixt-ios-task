//
//  MapViewController.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/7/21.
//

import UIKit
import MapKit
import FloatingPanel

class MapViewController: UIViewController {
	// MARK: - Outlets
	
	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var collectionView: UICollectionView!
	
	// MARK: - Constants
	let collectionViewHeight: CGFloat = 200
	
	// MARK: - Variables
	var viewModel: MapViewModel!
	var floatingPanel: FloatingPanelController?
	
	private var collectionViewDataSource: CarsCollectionViewDataSource<CarCollectionViewCell, MapItemViewModel>?
	
	// MARK: - Lifesycle
	
	static func create(viewModel: MapViewModel) -> MapViewController {
		let controller = MapViewController.loadFromNib()
		
		controller.viewModel = viewModel
		
		return controller
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.view.accessibilityIdentifier = AccessibilityIdentifier.mapViewController
		self.mapView.delegate = self
		self.configureCollection()
		self.bind(to: viewModel)
		self.viewModel.load()
    }
	
	private func configureCollection() {
		self.collectionView.register(
			UINib(
				nibName: CarCollectionViewCell.identifier,
				bundle: nil),
			forCellWithReuseIdentifier: CarCollectionViewCell.identifier)
	}
	
	// MARK: - Private functions
	
	private func bind(to viewModel: MapViewModel) {
		
		viewModel.error.observe(on: self) { [weak self] error in
			self?.showErrorAlert(errorMessage: error)
		}
		
		viewModel.items.observe(on: self) { [weak self] items in
			self?.updateAnnotations(items)
			self?.updateCollectionViewDatasource(items)
		}
		
		viewModel.selectedItem.observe(on: self) { [weak self] selectedItem in
			self?.updateSelectedAnnotation(selectedItem)
			self?.updateCollectionViewforSelectedItem(at: viewModel.selectedItemIndex)
		}
		
		viewModel.isListShown.observe(on: self) { [weak self] isListShown in
			self?.showList(isListShown: isListShown)
		}
	}
	
	private func updateSelectedAnnotation(_ annotation: MKAnnotation?) {
		
		if let annotation = annotation {
			self.mapView.setCenter(annotation.coordinate, animated: true)
			
			if !(self.mapView.view(for: annotation)?.isSelected ?? false) {
				self.mapView.selectAnnotation(annotation, animated: true)
			}
			
		} else {
			for annotation in self.mapView.annotations {
				self.mapView.deselectAnnotation(annotation, animated: true)
			}
			
		}
	}
	
	private func updateCollectionViewDatasource(_ items: [MapItemViewModel]) {
		
		self.collectionViewDataSource = CarsCollectionViewDataSource(items: items, bind: { (cell, item) in
			cell.bind(item: item)
		})
		
		self.collectionView.dataSource = self.collectionViewDataSource
		self.collectionView.reloadData()
	}
	
	private func updateCollectionViewforSelectedItem(at index: Int?) {
		
		if let index = index {
			let wasHidden = self.collectionView.isHidden
			
			self.collectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: !wasHidden)
			
			if wasHidden {
				self.collectionView.alpha = 0
				self.collectionView.isHidden = false
				
				
				UIView.animate(withDuration: 0.3, animations: {
					self.collectionView.alpha = 1
				})
			}
		} else {
			self.collectionView.alpha = 1
			
			UIView.animate(withDuration: 0.3, animations: {
				self.collectionView.alpha = 0
			}) { done in
				self.collectionView.isHidden = true
			}
		}
	}
	
	private func updateAnnotations(_ annotations: [MKAnnotation]) {
		
		self.mapView.addAnnotations(annotations)
		
		self.mapView.showAnnotations(annotations, animated: false)
		
	}
	
	private func showErrorAlert(errorMessage: String) {
		if !errorMessage.isEmpty {
			let alertController = UIAlertController(title: NSLocalizedString("Error", comment: "Popup title"),
													message: errorMessage,
													preferredStyle: .alert)
			alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Popup button title"),
													style: .cancel,
													handler: nil))
			alertController.addAction(UIAlertAction(title: NSLocalizedString("Retry", comment: "Popup button title"),
													style: .default,
													handler: { (action) in
														self.viewModel.retry()
													}))
			
			self.present(alertController, animated: true)
		}
	}
	
	private func showList(isListShown: Bool) {
		if isListShown {
			if self.floatingPanel == nil {
				let floatingPanel = CarListViewController.createAsFloatingPanel(viewModel: self.viewModel)
				self.floatingPanel = floatingPanel
				self.present(floatingPanel, animated: true) {
					self.mapView.showAnnotations(self.viewModel.items.value, animated: false)
					self.setMapBottomMargin(UIScreen.main.bounds.height * 0.5)
				}
			}
		} else {
			if self.floatingPanel != nil {
				self.floatingPanel?.dismiss(animated: true)
				self.floatingPanel = nil
				self.mapView.showAnnotations(self.viewModel.items.value, animated: self.viewModel.selectedItem.value == nil)
			}
		}
	}
	
	private func setMapBottomMargin(_ inset: CGFloat) {
		OperationQueue.main.addOperation {
			self.mapView.setVisibleMapRect(self.mapView.visibleMapRect,
										   edgePadding: UIEdgeInsets(top: 0,
																	 left: 0,
																	 bottom: inset,
																	 right: 0),
										   animated: true)
			
		}
	}
	
}

// MARK: - MKMapViewDelegate implementation
extension MapViewController: MKMapViewDelegate {
	func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		
		let reuseId = "car_pin"
		
		var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKMarkerAnnotationView
		if pinView == nil {
			pinView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
			pinView?.accessibilityIdentifier = AccessibilityIdentifier.mapAnnotationView
			if let pinView = pinView {
				mapView.accessibilityElements?.append(pinView)
			}
		}
		pinView?.animatesWhenAdded = true
		pinView?.glyphImage = UIImage(named:"mapPinGlyphIcon")
		pinView?.annotation = annotation
		pinView?.glyphTintColor = UIColor.backgroundColor
		pinView?.markerTintColor = UIColor.accentColor
		
		return pinView
	}
	
	func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
		if let item = view.annotation as? MapItemViewModel {
			self.viewModel.didSelect(item: item)
		}
	}
	
	func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
		if mapView.selectedAnnotations.isEmpty {
			self.viewModel.didDeselect()
		}
	}
	
}

// MARK: - UICollectionViewDelegate & UICollectionViewDelegateFlowLayout implementation
extension MapViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
		return CGSize(width: UIScreen.main.bounds.width, height: self.collectionViewHeight)
	}
	
	func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		let index = Int(self.collectionView.contentOffset.x / self.collectionView.frame.size.width)
		
		if self.viewModel.selectedItemIndex != index {
			self.viewModel.didSelectItem(at: index)
		}
		
	}
}
