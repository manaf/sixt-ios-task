//
//  CarListViewController.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/8/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit
import FloatingPanel

class CarListViewController: UIViewController {
	// MARK: - Outlets
	
	@IBOutlet weak var tableView: UITableView!
	
	// MARK: - Variables
	
	var viewModel: MapViewModel!
	private var tableViewDataSource: CarsTableViewDataSource<CarTableViewCell, MapItemViewModel>?
	
	// MARK: - Lifecycle
	
	static func createAsFloatingPanel(viewModel: MapViewModel) -> FloatingPanelController {
		let controller = CarListViewController.create(viewModel: viewModel)
		let floatingPanel = FloatingPanelController()
		floatingPanel.set(contentViewController: controller)
		floatingPanel.backdropView.backgroundColor = UIColor.backgroundColor
		floatingPanel.surfaceView.backgroundColor = UIColor.backgroundColor
		floatingPanel.track(scrollView: controller.tableView)
		return floatingPanel
	}
	
	static func create(viewModel: MapViewModel) -> CarListViewController {
		let controller = CarListViewController.loadFromNib()
		
		controller.viewModel = viewModel
		return controller
	}
	
	override func viewDidLoad() {
        super.viewDidLoad()
		self.view.accessibilityIdentifier = AccessibilityIdentifier.carListViewController
		self.view.backgroundColor = UIColor.backgroundColor
		self.bind(self.viewModel)
		self.configureTable()
    }

	// MARK: - Private functions
	
	private func configureTable() {
		self.tableView.register(
			UINib(
				nibName: CarTableViewCell.identifier,
				bundle: nil),
			forCellReuseIdentifier: CarTableViewCell.identifier)
	}
	
	private func bind(_ viewModel: MapViewModel) {
		
		viewModel.items.observe(on: self) { [weak self] items in
			
			self?.updateTableViewDataSource(items)
		}
	}
	
	private func updateTableViewDataSource(_ items: [MapItemViewModel]) {
		self.tableViewDataSource = CarsTableViewDataSource(items: items, bind: { (cell, item) in
			cell.bind(item: item)
		})
		
		self.tableView.dataSource = self.tableViewDataSource
		self.tableView.reloadData()
	}
	
}

extension CarListViewController: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		self.viewModel.didSelectItem(at: indexPath.row)
	}
}
