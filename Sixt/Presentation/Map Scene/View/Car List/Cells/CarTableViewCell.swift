//
//  CarTableViewCell.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/8/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit
import Kingfisher

class CarTableViewCell: UITableViewCell {

	@IBOutlet weak var itemImageView: UIImageView!
	@IBOutlet weak var itemTitleLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		self.itemTitleLabel.textColor = UIColor.contentColor
	}
	
	func bind(item: MapItemViewModel) {
		self.itemTitleLabel.text = item.title
		
		if let url = item.imageURL {
			KF.url(url)
				.placeholder(UIImage(named: "carPlaceholder"))
				.set(to: self.itemImageView)
		}
		
	}
}
