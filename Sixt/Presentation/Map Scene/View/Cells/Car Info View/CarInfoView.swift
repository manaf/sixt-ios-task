//
//  CarInfoView.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit

class CarInfoView: UIView {

	@IBOutlet weak var itemImageView: UIImageView!
	@IBOutlet weak var itemTextLabel: UILabel!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.itemImageView.tintColor = UIColor.accentColor
		self.itemTextLabel.textColor = UIColor.contentColor
	}
	
	func bind(item: MapItemInfoViewModel) {
		self.itemImageView.image = item.image
		self.itemTextLabel.text = item.text
	}
	
}
