//
//  CarCollectionViewCell.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import UIKit
import Kingfisher

class CarCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var itemImageView: UIImageView!
	@IBOutlet weak var itemTitleLabel: UILabel!
	@IBOutlet weak var itemSubtitleLabel: UILabel!
	@IBOutlet weak var stackView: UIStackView!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		self.itemTitleLabel.textColor = UIColor.contentColor
		self.itemSubtitleLabel.textColor = UIColor.contentColor
	}
	
	func bind(item: MapItemViewModel) {
		self.itemTitleLabel.text = item.title
		self.itemSubtitleLabel.text = item.subtitle
		
		if let url = item.imageURL {
			KF.url(url)
				.placeholder(UIImage(named: "carPlaceholder"))
				.set(to: self.itemImageView)
		}
		
		self.stackView.arrangedSubviews.forEach {
			self.stackView.removeArrangedSubview($0)
			NSLayoutConstraint.deactivate($0.constraints)
			$0.removeFromSuperview()
		}
		
		for infoItem in item.infoItems {
			let view: CarInfoView = .fromNib()
			view.bind(item: infoItem)
			self.stackView.addArrangedSubview(view)
		}
		
	}
}
