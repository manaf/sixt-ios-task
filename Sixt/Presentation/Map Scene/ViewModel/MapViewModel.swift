//
//  MapViewModel.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/8/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation

// MARK: - MapViewModel abstractions

protocol MapViewModelInput {
	func load()
	func retry()
	func didSelectItem(at index: Int)
	func didSelect(item: MapItemViewModel)
	func didDeselect()
}

protocol MapViewModelOutput {
	var items: Observable<[MapItemViewModel]> { get }
	var selectedItem: Observable<MapItemViewModel?> { get }
	var selectedItemIndex: Int? { get }
	var isListShown: Observable<Bool> { get }
	var loading: Observable<Bool> { get }
	var error: Observable<String> { get }
	var isEmpty: Bool { get }
}

protocol MapViewModel: MapViewModelInput, MapViewModelOutput {}

// MARK: - MapViewModel Implemmentation
final class MapViewModelImp: MapViewModel {
	
	private let fetchCarsUseCase: FetchCarsUseCase
	
	// MARK: - MapViewModelOutput variables
	var items: Observable<[MapItemViewModel]> = Observable([])
	var selectedItem: Observable<MapItemViewModel?> = Observable(nil)
	var selectedItemIndex: Int?
	var isListShown: Observable<Bool> = Observable(false)
	var loading: Observable<Bool> = Observable(false)
	var error: Observable<String> = Observable("")
	var isEmpty: Bool = false
	
	init(fetchCarsUseCase: FetchCarsUseCase) {
		self.fetchCarsUseCase = fetchCarsUseCase
	}
	
	// MARK: - MapViewModelInput functions
	func load() {
		self.fetch()
	}
	
	func retry() {
		self.fetch()
	}
	
	func didSelectItem(at index: Int) {
		if self.isListShown.value == true {
			self.isListShown.value = false
		}
		self.selectedItemIndex = index
		self.selectedItem.value = self.items.value[index]
	}
	
	func didSelect(item: MapItemViewModel) {
		if self.isListShown.value == true {
			self.isListShown.value = false
		}
		self.selectedItemIndex = self.items.value.firstIndex(of: item)
		self.selectedItem.value = item
	}
	
	func didDeselect() {
		self.isListShown.value = true
		self.selectedItemIndex = nil
		self.selectedItem.value = nil
	}
	
	// MARK: - MapViewModel private functions
	
	private func fetch() {
		self.fetchCarsUseCase.execute { (result) in
			switch result {
			
			case .success(let cars):
				self.isListShown.value = true
				self.items.value = cars.map{ MapItemViewModel(car: $0) }
				
			case .failure(let error):
				self.error.value = error.isConnectionError ? NSLocalizedString("Your internet connection is unavailable.", comment: "A message for connection errors.") : NSLocalizedString("Something went wrong.", comment: "A generic message for API errors.")
			}
		}
	}
}
