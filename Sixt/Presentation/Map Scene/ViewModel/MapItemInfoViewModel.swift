//
//  MapItemInfoViewModel.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import UIKit

final class MapItemInfoViewModel {
	
	let text: String
	let image: UIImage?
	
	init(text: String, image: UIImage?) {
		self.text = text
		self.image = image
	}
	
}
