//
//  CarsTableViewDataSource.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import UIKit

class CarsTableViewDataSource<Cell: UITableViewCell, Item>: NSObject, UITableViewDataSource {
	
	let items: [Item]
	let bind: (Cell, Item) -> Void
	
	init(items: [Item], bind: @escaping (Cell, Item)->Void) {
		self.items = items
		self.bind = bind
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.items.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
		
		self.bind(cell, self.items[indexPath.item])
		
		return cell
	}
	
}
