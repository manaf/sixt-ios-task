//
//  CarsCollectionViewDataSource.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/11/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import UIKit

class CarsCollectionViewDataSource<Cell: UICollectionViewCell, Item>: NSObject, UICollectionViewDataSource {
	
	let items: [Item]
	let bind: (Cell, Item) -> Void
	
	init(items: [Item], bind: @escaping (Cell, Item)->Void) {
		self.items = items
		self.bind = bind
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.items.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Cell.identifier, for: indexPath) as! Cell
		
		self.bind(cell, self.items[indexPath.item])
		
		return cell
	}
	
	
	
}
