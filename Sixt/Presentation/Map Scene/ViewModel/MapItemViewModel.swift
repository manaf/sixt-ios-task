//
//  MapItemViewModel.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import MapKit

final class MapItemViewModel: NSObject, MKAnnotation {
	
	let id: String
	let title: String?
	let subtitle: String?
	let coordinate: CLLocationCoordinate2D
	let imageURL: URL?
	let infoItems: [MapItemInfoViewModel]
	
	init(car: Car) {
		self.id = car.id
		self.title = "\(car.make ?? "") \(car.series ?? "")"
		self.subtitle = car.name
		self.coordinate = CLLocationCoordinate2D(latitude: car.latitude, longitude: car.longitude)
		self.imageURL = car.carImageUrl
		
		var infoItems: [MapItemInfoViewModel] = []
		
		//Create Info Item from Transmition
		if let transmition = car.transmission {
			var text: String
			var image: UIImage?
			
			switch transmition {
			case .automatic:
				text = NSLocalizedString("Automatic", comment: "As in a car has Automatic transmition")
				image = UIImage(named: "autoTransmitionIcon")
			case .manual:
				text = NSLocalizedString("Manual", comment: "As in a car has Automatic transmition")
				image = UIImage(named: "manualTransmitionIcon")
			}
			
			infoItems.append(MapItemInfoViewModel(text: text, image: image))
		}
		
		//Create Info Item from FuleType and FuelLevel
		if let fuelType = car.fuelType, let fuelLevel = car.fuelLevel {
			let text: String = fuelLevel.percentageString
			var image: UIImage?
			
			switch fuelType {
			case .diesel:
				image = UIImage(named: "dieselIcon")
			case .petrol:
				image = UIImage(named: "petrolIcon")
			case .electric:
				image = UIImage(named: "batteryIcon")
			}
			
			infoItems.append(MapItemInfoViewModel(text: text, image: image))
		}
		
		//Create Info Item from InnerCleanliness
		if let cleanliness = car.innerCleanliness {
			var text: String
			let image: UIImage? = UIImage(named: "cleanlinessIcon")
			
			switch cleanliness {
			case .regular:
				text = NSLocalizedString("Regular", comment: "Refers to a cars cleanliness")
			case .clean:
				text = NSLocalizedString("Clean", comment: "Refers to a cars cleanliness")
			case .veryClean:
				text = NSLocalizedString("Very clean", comment: "Refers to a cars cleanliness")
			}
			
			infoItems.append(MapItemInfoViewModel(text: text, image: image))
		}
		
		self.infoItems = infoItems
	}
}
