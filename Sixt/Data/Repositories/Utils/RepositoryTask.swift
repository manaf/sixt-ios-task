//
//  RepositoryTask.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import SENetworking
class RepositoryTask: Cancellable {
	var networkTask: NetworkCancellable?
	var isCancelled: Bool = false
	
	func cancel() {
		networkTask?.cancel()
		isCancelled = true
	}
}
