//
//  CarsRepositoryImpl.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import SENetworking

final class CarsRepositoryImpl {
	private let dataTransferService: DataTransferService
	
	init(dataTransferService: DataTransferService) {
		self.dataTransferService = dataTransferService
	}
}

extension CarsRepositoryImpl: CarsRepository {
	
	@discardableResult
	func fetchCarsList(completion: @escaping (Result<[Car], Error>) -> Void) -> Cancellable? {
		
		let task = RepositoryTask()
		let endpoint = APIEndpoints.getCars()
		
		task.networkTask = self.dataTransferService.request(with: endpoint) { result in
			switch result {
			case .success(let responseDTO):
				completion(.success(responseDTO.map{ $0.toDomain()}))
			case .failure(let error):
				completion(.failure(error))
			}
		}
		
		return task
		
	}
}
