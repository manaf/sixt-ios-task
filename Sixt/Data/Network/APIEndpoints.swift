//
//  APIEndpoints.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import SENetworking

struct APIEndpoints {
	
	static func getCars() -> Endpoint<[CarDTO]> {
		return Endpoint(path: "codingtask/cars",
						method: .get)
	}
}
