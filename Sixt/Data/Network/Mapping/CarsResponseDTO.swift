//
//  CarsResponseDTO.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation

// MARK: - Data Transfer Objects
struct CarDTO: Decodable {
	
	enum CodingKeys: String, CodingKey {
		case id = "id"
		case modelIdentifier = "modelIdentifier"
		case modelName = "modelName"
		case name = "name"
		case make = "make"
		case group = "group"
		case color = "color"
		case series = "series"
		case fuelType = "fuelType"
		case fuelLevel = "fuelLevel"
		case transmission = "transmission"
		case licensePlate = "licensePlate"
		case latitude = "latitude"
		case longitude = "longitude"
		case innerCleanliness = "innerCleanliness"
		case carImageUrl = "carImageUrl"
	}
	
	enum FuelTypeDTO: String, Decodable {
		case petrol = "P"
		case diesel = "D"
		case electric = "E"
	}
	
	enum TransmitionDTO: String, Decodable {
		case manual = "M"
		case automatic = "A"
	}
	
	enum InnerCleanlinessDTO: String, Decodable {
		case regular = "REGULAR"
		case clean = "CLEAN"
		case veryClean = "VERY_CLEAN"
	}
	
	var id: String
	var modelIdentifier: String?
	var modelName: String?
	var name: String?
	var make: String?
	var group: String?
	var color: String?
	var series: String?
	var fuelType: FuelTypeDTO?
	var fuelLevel: Double?
	var transmission: TransmitionDTO?
	var licensePlate: String?
	var latitude: Double!
	var longitude: Double!
	var innerCleanliness: InnerCleanlinessDTO?
	var carImageUrl: URL?
}

// MARK: - Mappings to Domain

extension CarDTO {
	func toDomain() -> Car {
		return Car(id: self.id,
				   modelIdentifier: self.modelIdentifier,
				   modelName: self.modelName,
				   name: self.name,
				   make: self.make,
				   group: self.group,
				   color: self.color,
				   series: self.series,
				   fuelType: self.fuelType?.toDomain(),
				   fuelLevel: self.fuelLevel,
				   transmission: self.transmission?.toDomain(),
				   licensePlate: self.licensePlate,
				   latitude: self.latitude,
				   longitude: self.longitude,
				   innerCleanliness: self.innerCleanliness?.toDomain(),
				   carImageUrl: self.carImageUrl)
	}
}

extension CarDTO.FuelTypeDTO {
	func toDomain() -> Car.FuelType {
		switch self {
		
		case .petrol:
			return .petrol
		case .diesel:
			return .diesel
		case .electric:
			return .electric
		}
	}
}

extension CarDTO.InnerCleanlinessDTO {
	func toDomain() -> Car.InnerCleanliness {
		switch self {
		
		case .regular:
			return .regular
		case .clean:
			return .clean
		case .veryClean:
			return .veryClean
		}
	}
}

extension CarDTO.TransmitionDTO {
	func toDomain() -> Car.Transmition {
		switch self {
		
		case .manual:
			return .manual
		case .automatic:
			return .automatic
		}
	}
}
