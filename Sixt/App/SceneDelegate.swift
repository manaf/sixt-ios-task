//
//  SceneDelegate.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/8/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	let diContainer = DIContainer()
	var appFlowCoordinator: AppFlowCoordinator?
	var window: UIWindow?
	
	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		
		if let windowScene = (scene as? UIWindowScene) {
			let window = UIWindow(windowScene: windowScene)
			let navigationController = UINavigationController()
			navigationController.navigationBar.isHidden = true
			
			window.rootViewController = navigationController
			appFlowCoordinator = AppFlowCoordinator(navigationController: navigationController,
													diContainer: self.diContainer)
			appFlowCoordinator?.start()
			window.makeKeyAndVisible()
			self.window = window
		}
	}
	
}

