//
//  DIContainer.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import SENetworking

final class DIContainer {
	
	let appConfiguration = AppConfiguration()
	
	// MARK: - Networking
	
	lazy var dataTransferService: DataTransferService = {
		guard let baseURL = URL(string: appConfiguration.apiBaseURL) else {
			fatalError("Invalid API base URL")
		}
		
		let config = ApiDataNetworkConfig(baseURL: baseURL)
		
		let dataNetwork = DefaultNetworkService(config: config)
		return DefaultDataTransferService(with: dataNetwork)
	}()
	
	// MARK: - DIContainers for Scenes
	
	func createMapSceneDIContainer() -> MapSceneDIContainer {
		return MapSceneDIContainer(dataTransferService: self.dataTransferService)
	}
}
