//
//  MapSceneDIContainer.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation
import SENetworking

final class MapSceneDIContainer {
	
	let dataTransferService: DataTransferService
	
	init(dataTransferService: DataTransferService) {
		self.dataTransferService = dataTransferService
	}
	
	// MARK: - Use Cases
	
	func createFetchCarsUseCase() -> FetchCarsUseCase {
		return FetchCarsUseCaseImpl(carsRepository: self.createCarsRepository())
	}
	
	// MARK: - Repositories
	
	func createCarsRepository() -> CarsRepository {
		return CarsRepositoryImpl(dataTransferService: self.dataTransferService)
	}
	
	// MARK: - Views
	
	func createMapViewController() -> MapViewController {
		return MapViewController.create(viewModel: self.createMapViewModel())
	}
	
	// MARK: - View Models
	
	func createMapViewModel() -> MapViewModel {
		return MapViewModelImp(fetchCarsUseCase: self.createFetchCarsUseCase())
	}
}
