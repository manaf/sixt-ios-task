//
//  FlowCoordinator.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/10/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation

import UIKit

final class AppFlowCoordinator {

	var navigationController: UINavigationController
	private let diContainer: DIContainer
	
	init(navigationController: UINavigationController,
		 diContainer: DIContainer) {
		self.navigationController = navigationController
		self.diContainer = diContainer
	}
	
	func start() {
		let mapSceneDIContainer = diContainer.createMapSceneDIContainer()
		navigationController.viewControllers = [mapSceneDIContainer.createMapViewController()]
	}
}
