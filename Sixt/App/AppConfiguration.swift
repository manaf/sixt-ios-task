//
//  AppConfiguration.swift
//  Sixt
//
//  Created by Manaf Abd Alrahim on 1/9/21.
//  Copyright © 2021 Manaf Alabd Alrahim. All rights reserved.
//

import Foundation

final class AppConfiguration {
	
	lazy var apiBaseURL: String = {
		return self.getStringVal(for: "apiBaseURL")
	}()
	
	// Dictionary with values of Config.plist
	private var configDict: [String : Any]?
	
	// Initializer gets the config file and stores it in a dictionary
	init() {
		self.configDict = self.getTargetConfigFile()
	}
	
	// Get the value of a string configuration
	private func getStringVal(for key: String) -> String {
		guard let val = self.configDict?[key] as? String, !val.isEmpty else {
			fatalError("Couldn't find key '\(key)' in 'Config.plist'.")
		}
		return val
	}
	
	// Get config file as dictionary
	private func getTargetConfigFile() -> [String : Any] {
		
		var result = [String : Any]()
		
		if let filePath = Bundle.main.path(forResource: "Config", ofType: "plist"),
			
			let pictionary = NSDictionary(contentsOfFile: filePath) {
			
			for (key, value) in pictionary {
				
				if let key = key as? String{
					
					result[key] = value
					
				}
				
			}
			
		}
		
		return result
		
	}
}

